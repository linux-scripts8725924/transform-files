# Transform files

This is a script bash to transform any document file, it should only accept document files as inputs and it outputs the transformed files. First bash script developed from scratch, there are things to change and any feedback is appreciated for improving this code.

If you want to execute that, just download the file, then open the command line and write any of this commands:

sudo chmod u+x transformfiles.sh  

or:

sudo chmod 777 transformfiles.sh

and then just execute the file as:

bash transformfiles.sh  

or

./transformfiles.sh


Notes: This was designed to work for ArchLinux-based distros, however I want to improve functionality for all popular Linux distros at least. 

When writing the path to the location where your document is, please try writing the whole path (home/username/...) instead of $HOME/ or home/ since I still need to develop a better way to look for the file instead of moving to the file location.

This script will be in GPLv3 license, so that everyone can freely use this and modify that. Just make sure to leave comments about any modifications. Thank you