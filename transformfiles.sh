#!/bin/bash
#Command for exceptions to handle user input errors

#Credits to DaosomLerri for this method

exceptions () {
# set is for declaring an exception in bash, the +e and -e are the "names" of the exceptions, but isn't naming them with a complex name
	set +e 
	error=$1
# -z "$variable" is a condition where it detects if a variable is empty
	[ -z "$error" ] && { echo -e "This value can't be found or it's empty, please try again, exiting the program";	exit 1; }
	set -e 
}

# credits to the link https://bash.cyberciti.biz/guide/Pass_arguments_into_a_function for the next 2 functions I used 

#it detects if the variable contains a string of an existing directory

is_directory () {
	set +e

#$1 means the first parameter  passed to a function
	diraddr=$1

# -d means directory
#
	[ -d "$diraddr" ] && { echo "Redirecting to that directory... "; cd "$diraddr"; } || { echo -e "That's not a directory, write the correct path to your file by your preferred file admin"; exit 1; }
	set -e
}

# it detects if the variable contains a string of an existing file in the current directory

is_file () {
	set +e 
	fileconv=$1
	[ -f "$fileconv" ] && echo "File exists, proceeding to transform it... " || { echo -e "The file doesn't exists"; exit 1;}
	set -e
}

echo "Hello, this is a simple way to transform files, the only thing you need
is having libreoffice installed on your pc"

echo  "Let's find out if you have it installed"
#-e used to find a file, the next will be the path of the file
#i.e: if [ -e /usr/bin/libreoffice ]
# $? !=0 used when a command equals 0
#i.e: if [[$? != 0]] 

echo "Searching program being installed in your machine..."

librefile=$(which libreoffice)

sleep 2s

#function for installing LibreOffice when needed and wanted
inLO ()
{
       	read -p  "Do you want to install Libreoffice? Yes or no? " DEC
	[ -z "$DEC" ] && exit 1 || echo "Answer received!"
	if [[ ${DEC,,} -eq yes ]]
       	then
		echo "You are about to get libreoffice on your laptop or PC"
		sleep 1s
		sudo pacman -S --noconfirm libreoffice
	else
		exit
	fi
}

if [[ $librefile ]];
then
	echo -e "LibreOffice is found"
else 
	echo -e "LibreOffice isn't found"
	inLO
fi

#function for executing the command soffice with input and output file, and redirecting to the location of input file
filedec() 
{
	read -p "Write the exact path where your file is located: " PATHFILE
# solving bug of exceptions var where it only exited because the parameter $1 was empty
	exceptions "$PATHFILE"

	is_directory "$PATHFILE"

	sleep 1s

	echo "Now, you are at "$PATHFILE""

	sleep 1s

	read -p "Write the type of file you want to be converted to: " FILETYPE

	exceptions "$FILETYPE"

	echo  "You've chosen ."${FILETYPE,,}""
	
       	read -p "Write the exact name of your file, including its original extension, the original extension in lower case: " ORIFILE

	exceptions "$ORIFILE"

	is_file "$ORIFILE"

	echo "Making new file with ."$FILETYPE" extension"
	
	sleep 2s
	
	soffice --headless --convert-to "${FILETYPE,,}" "$ORIFILE"
} 

filedec

sleep 2s

echo "File is done"

sleep 3s

echo "Good bye"

sleep 1s

clear


#function for installing neofetch when required by the user

nfetchinstall () {
	NEOEXIST=$(which neofetch)
	if [[ $NEOEXIST ]]; 
	then
		neofetch
	else
		read -p "Do you want to install neofetch? (Y/n)" nfetchinstall
		[[ -z $nfetchinstall ]] && exit || echo "Answer received, making actions..."
		if [[ ${nfetchinstall,,} -eq y ]] 
		then
			sudo pacman -S --noconfirm neofetch
		else
			clear
		fi
	fi
}

nfetchinstall 





























